﻿using System;

namespace VowelCountTask
{
    public static class StringHelper
    {
        public static int GetCountOfVowel(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentException("Source string is null or empty.");
            }

            int count = 0;
            char[] vowels = { 'a', 'e', 'i', 'o', 'u' };

            for (int i = 0; i < source.Length; i++)
            {
                for (int j = 0; j < vowels.Length; j++)
                {
                    if (source[i] == vowels[j])
                    {
                        count++;
                        break;
                    }
                }
            }

            return count;
        }
    }
}
